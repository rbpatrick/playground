<?php
    // 2. Perform database query
    $result  = db_select("subjects", "visible", 1);
?>

<!DOCTYPE html>
<html>
    <head>
        <meta lang="en">
        <meta name="keywords" content="<?php echo $keywords; ?>">
        <link rel="stylesheet" type="text/css" href="css/styles.css">
        <title><?php echo $title; ?></title>
    </head>
    <body>
        <header>
            <nav>
                <ul>
                    <?php
                        // 3. Use returned data (if any)
                        while($content = mysqli_fetch_assoc($result)) {
                        // output data from each row
                    ?>
                    <li><a href="?id=<?php echo urlencode($content['id']); ?>"><?php echo ucfirst($content['menu_name']); ?></a></li>
                    <?php
                        }

                        // 4. Release returned data
                        mysqli_free_result($result);
                    ?>
                </ul>
            </nav>
        </header>
        <main>
            