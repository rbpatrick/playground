<?php
    /**
     * Render Template
     *
     * @param array $data
     */
    function render($template, $data = array()) {
        $path = "../views/" . $template . ".php";
        if (file_exists($path)) {
            foreach ($data as $key => &$value) {
                $value = htmlspecialchars($value);
            }
            extract($data);
            require($path);
        }
    }

    /* Redirect to a different page */
    function redirect_to($location) {
        header("Location: " . $location);
        exit;
    }

    function find_all_subjects() {
        global $db;
        
        $query  = "SELECT * ";
        $query .= "FROM subjects ";
        $query .= "WHERE visible = 1 ";
        $query .= "ORDER BY position ASC";
        $subject_set = mysqli_query($db, $query);
        confirm_query($subject_set);
        return $subject_set;
    }

    function find_pages_for_subject($subject_id) {
        global $db;
        
        $query  = "SELECT * ";
        $query .= "FROM pages ";
        $query .= "WHERE visible = 1 ";
        $query .= "AND subject_id = {$subject_id} ";
        $query .= "ORDER BY position ASC";
        $subject_set = mysqli_query($db, $query);
        confirm_query($subject_set);
        return $result;
    }

    function find_page_for_subject($subject_id, $menu_name) {
        global $db;
        
        $query  = "SELECT * ";
        $query .= "FROM pages ";
        $query .= "WHERE visible = 1 ";
        $query .= "AND subject_id = {$subject_id} ";
        $query .= "AND menu_name = {$menu_name} ";
        $query .= "ORDER BY position ASC";
        $subject_set = mysqli_query($db, $query);
        confirm_query($subject_set);
        return $result;
    }
?>