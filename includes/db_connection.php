<?php
    // Database constents
    define("DB_HOST", "localhost");
    define("DB_USER", "webmaster");
    define("DB_PASS", "webmaster");
    define("DB_NAME", "playground");
    // Create a database connection
    $db = mysqli_connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);
    // Test if connection succeeded
    if (mysqli_connect_errno()) {
        printf("Connect failed: %s\n", mysqli_connect_error());
        exit();
    }
?>

<?php
    // Confirm query has brought back result 
    function confirm_query($result_set) {
        if (!$result_set) {
            die("Database query failed.");
        }
    }
    
    // Database CRUD
    // INSERT (Create database content)
    function db_insert($table, $column_list, $value_list) {
        global $db;
        
        $query  = "INSERT INTO {$table} (";
        $query .= " {$column_list} ";
        $query .= ") VALUES (";
        $query .= " {$value_list} ";
        $query .= ")";
        
        $result = mysqli_query($db, $query);
        // Test if there was a query error
        if ($result) {
            // Success
            // redirect_to(somepage.php");
            echo "Success!";
        } else {
            // Falure
            // $message = "Subject creation failed";
            die("Database query failed. " . mysqli_error($db));
        }
    }
    // SELECT (Read database content)
    function db_select($table, $column, $column_value, $order = "ASC", $select = "*") {
        global $db;
        
        $query  = "SELECT {$select} ";
        $query .= "FROM {$table} ";
        $query .= "WHERE {$column} = {$column_value} ";
        $query .= "ORDER BY position {$order}";
        $result = mysqli_query($db, $query);
        confirm_query($result);
        return $result;
    }
    // UPDATE (Update database content)
    function db_update($table, $column, $new_column_value, $id) {
        $query  = "UPDATE {$table} ";
        $query .= "SET {$column} = {$new_column_value} ";
        $query .= "WHERE id = {$id}";
        $db_update = mysqli_query($db, $query);
        confirm_query($db_update);
    }
    // DELETE (Delete database content)
    function db_delete($table, $column_name, $column_value) {
        $query  = "DELETE FROM {$table} ";
        $query .= "WHERE {$column_name} = {$column_value}";
        $db_delete = mysqli_query($db, $query);
        confirm_query($db_delete);
    }
?>